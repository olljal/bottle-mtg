# bottle-mtg

MTG card browser with Bottle

https://bottlepy.org/docs/dev/

Start server with:

 `python3 main.py` 

with gunicorn:

`gunicorn -w 2 main:main`

Test version available at:
https://magictesti.herokuapp.com/